EESchema Schematic File Version 4
LIBS:TSAL_Hauptplatine-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R9
U 1 1 5BD83C49
P 9250 4250
F 0 "R9" H 9320 4296 50  0000 L CNN
F 1 "620" H 9320 4205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9180 4250 50  0001 C CNN
F 3 "~" H 9250 4250 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-620-ohm-125-mw-1-rnd-0805-1-620-p183191.html?&trstct=pos_1" H 9250 4250 50  0001 C CNN "Bst"
	1    9250 4250
	-1   0    0    1   
$EndComp
Text GLabel 10350 4150 0    50   Input ~ 0
+24V
Text GLabel 10350 4550 0    50   Input ~ 0
+24V
Text GLabel 10350 4450 0    50   Input ~ 0
GND
Text GLabel 10350 4850 0    50   Input ~ 0
GND
Text GLabel 9000 4400 0    50   Input ~ 0
GND
Text GLabel 8950 4850 0    50   Input ~ 0
GND
Text GLabel 9000 4100 0    50   Input ~ 0
Measurement_1
Wire Wire Line
	9400 4400 9250 4400
Wire Wire Line
	9250 4400 9000 4400
Wire Wire Line
	9400 4100 9250 4100
Wire Wire Line
	9000 4100 9250 4100
Wire Wire Line
	9400 4750 9400 4850
Wire Wire Line
	9400 4550 9400 4650
Wire Wire Line
	9400 4850 9250 4850
Wire Wire Line
	9400 4550 9250 4550
$Comp
L Device:R R10
U 1 1 5BD84301
P 9250 4700
F 0 "R10" H 9320 4746 50  0000 L CNN
F 1 "620" H 9320 4655 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9180 4700 50  0001 C CNN
F 3 "~" H 9250 4700 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-620-ohm-125-mw-1-rnd-0805-1-620-p183191.html?&trstct=pos_1" H 9250 4700 50  0001 C CNN "Bst"
	1    9250 4700
	-1   0    0    1   
$EndComp
Connection ~ 9250 4850
Wire Wire Line
	9250 4850 8950 4850
Connection ~ 9250 4550
Wire Wire Line
	9250 4550 8950 4550
Text GLabel 8950 4550 0    50   Input ~ 0
Measurement_2
$Comp
L Regulator_Linear:L7805 U1
U 1 1 5BD88415
P 5750 6900
F 0 "U1" H 5750 7142 50  0000 C CNN
F 1 "L78M05CV" H 5750 7051 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220F-3_Horizontal_TabDown" H 5775 6750 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 5750 6850 50  0001 C CNN
	1    5750 6900
	1    0    0    -1  
$EndComp
Text GLabel 5200 6900 0    50   Input ~ 0
+24V
Text GLabel 6300 6900 2    50   Input ~ 0
+5V
$Comp
L Device:C C1
U 1 1 5BD8858C
P 5300 7050
F 0 "C1" H 5000 7050 50  0000 L CNN
F 1 "330nF" H 4950 6950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5338 6900 50  0001 C CNN
F 3 "~" H 5300 7050 50  0001 C CNN
F 4 "https://www.reichelt.de/vielschicht-kerko-330nf-50v-125-c-kem-x7r0805-330n-p207075.html?&trstct=pos_0" H 5300 7050 50  0001 C CNN "Bst"
	1    5300 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 6900 5300 6900
Wire Wire Line
	5300 6900 5450 6900
Wire Wire Line
	5300 7200 5750 7200
Wire Wire Line
	5750 7200 6200 7200
Connection ~ 5750 7200
Text GLabel 5750 7300 3    50   Input ~ 0
GND
Wire Wire Line
	5750 7200 5750 7300
Wire Wire Line
	6050 6900 6200 6900
$Comp
L Device:C C2
U 1 1 5BD89C3D
P 6200 7050
F 0 "C2" H 6400 7050 50  0000 L CNN
F 1 "100nF" H 6350 6950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6238 6900 50  0001 C CNN
F 3 "~" H 6200 7050 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-vielschicht-keramikkondensator-100n-10-x7r-g0805-100n-p31879.html?&trstct=pos_2" H 6200 7050 50  0001 C CNN "Bst"
	1    6200 7050
	1    0    0    -1  
$EndComp
Connection ~ 6200 6900
Wire Wire Line
	6200 6900 6300 6900
$Comp
L Device:Opamp_Quad_Generic U2
U 1 1 5BD8A153
P 1950 1300
F 0 "U2" H 1950 1667 50  0000 C CNN
F 1 "Opamp_Quad_Generic" H 1950 1576 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 1950 1300 50  0001 C CNN
F 3 "~" H 1950 1300 50  0001 C CNN
F 4 "https://de.rs-online.com/web/p/operationsverstarker/7681404/" H 1950 1300 50  0001 C CNN "Bst"
	1    1950 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:Opamp_Quad_Generic U2
U 2 1 5BD8A269
P 2000 2450
F 0 "U2" H 2000 2817 50  0000 C CNN
F 1 "Opamp_Quad_Generic" H 2000 2726 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 2000 2450 50  0001 C CNN
F 3 "~" H 2000 2450 50  0001 C CNN
	2    2000 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:Opamp_Quad_Generic U2
U 3 1 5BD8A2D6
P 1950 4550
F 0 "U2" H 1950 4917 50  0000 C CNN
F 1 "Opamp_Quad_Generic" H 1950 4826 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 1950 4550 50  0001 C CNN
F 3 "~" H 1950 4550 50  0001 C CNN
	3    1950 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:Opamp_Quad_Generic U2
U 4 1 5BD8A337
P 1950 5600
F 0 "U2" H 1950 5967 50  0000 C CNN
F 1 "Opamp_Quad_Generic" H 1950 5876 50  0000 C CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 1950 5600 50  0001 C CNN
F 3 "~" H 1950 5600 50  0001 C CNN
	4    1950 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:Opamp_Quad_Generic U2
U 5 1 5BD8A3AC
P 1150 7150
F 0 "U2" V 1200 7150 50  0000 L CNN
F 1 "Opamp_Quad_Generic" V 1300 6750 50  0000 L CNN
F 2 "Package_SO:TSSOP-14_4.4x5mm_P0.65mm" H 1150 7150 50  0001 C CNN
F 3 "~" H 1150 7150 50  0001 C CNN
	5    1150 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5BD8A83A
P 800 4500
F 0 "R5" H 870 4546 50  0000 L CNN
F 1 "10k" H 870 4455 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 730 4500 50  0001 C CNN
F 3 "~" H 800 4500 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-10-kohm-100-mw-1-smd-0603-10k-p89442.html?&trstct=pos_0" H 800 4500 50  0001 C CNN "Bst"
	1    800  4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5BD8ABF4
P 800 4800
F 0 "R6" H 870 4846 50  0000 L CNN
F 1 "39k" H 870 4755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 730 4800 50  0001 C CNN
F 3 "~" H 800 4800 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-39-kohm-100-mw-1-rnd-0603-1-39k-p183092.html?&trstct=pos_1" H 800 4800 50  0001 C CNN "Bst"
	1    800  4800
	1    0    0    -1  
$EndComp
Connection ~ 800  4650
Text GLabel 800  4350 1    50   Input ~ 0
+5V
Text GLabel 800  1500 3    50   Input ~ 0
GND
$Comp
L Device:R R7
U 1 1 5BD8B3B9
P 800 5550
F 0 "R7" H 870 5596 50  0000 L CNN
F 1 "10k" H 870 5505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 730 5550 50  0001 C CNN
F 3 "~" H 800 5550 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-10-kohm-100-mw-1-smd-0603-10k-p89442.html?&trstct=pos_0" H 800 5550 50  0001 C CNN "Bst"
	1    800  5550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5BD8B3C0
P 800 5850
F 0 "R8" H 870 5896 50  0000 L CNN
F 1 "39k" H 870 5805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 730 5850 50  0001 C CNN
F 3 "~" H 800 5850 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-39-kohm-100-mw-1-rnd-0603-1-39k-p183092.html?&trstct=pos_1" H 800 5850 50  0001 C CNN "Bst"
	1    800  5850
	1    0    0    -1  
$EndComp
Connection ~ 800  5700
Text GLabel 850  6600 0    50   Input ~ 0
+5V
Text GLabel 800  6000 3    50   Input ~ 0
GND
Text GLabel 1650 4450 0    50   Input ~ 0
Measurement_1
Wire Wire Line
	800  4650 1650 4650
Text GLabel 1650 5500 0    50   Input ~ 0
Measurement_2
Wire Wire Line
	800  5700 1650 5700
Text Label 2250 4550 0    50   ~ 0
>60V
Text Label 2250 5600 0    50   ~ 0
>60V
$Comp
L Device:R R4
U 1 1 5BD8D0FE
P 800 2500
F 0 "R4" H 870 2546 50  0000 L CNN
F 1 "30k" H 870 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 730 2500 50  0001 C CNN
F 3 "~" H 800 2500 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-30-kohm-100-mw-1-rnd-0603-1-30k-p183088.html?&trstct=pos_0" H 800 2500 50  0001 C CNN "Bst"
	1    800  2500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5BD8D138
P 800 2200
F 0 "R3" H 870 2246 50  0000 L CNN
F 1 "30k" H 870 2155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 730 2200 50  0001 C CNN
F 3 "~" H 800 2200 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-30-kohm-100-mw-1-rnd-0603-1-30k-p183088.html?&trstct=pos_0" H 800 2200 50  0001 C CNN "Bst"
	1    800  2200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5BD8D17F
P 800 1050
F 0 "R1" H 870 1096 50  0000 L CNN
F 1 "30k" H 870 1005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 730 1050 50  0001 C CNN
F 3 "~" H 800 1050 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-30-kohm-100-mw-1-rnd-0603-1-30k-p183088.html?&trstct=pos_0" H 800 1050 50  0001 C CNN "Bst"
	1    800  1050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5BD8D1C3
P 800 1350
F 0 "R2" H 870 1396 50  0000 L CNN
F 1 "30k" H 870 1305 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 730 1350 50  0001 C CNN
F 3 "~" H 800 1350 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-30-kohm-100-mw-1-rnd-0603-1-30k-p183088.html?&trstct=pos_0" H 800 1350 50  0001 C CNN "Bst"
	1    800  1350
	1    0    0    -1  
$EndComp
Text GLabel 800  2050 1    50   Input ~ 0
+5V
Text GLabel 800  4950 3    50   Input ~ 0
GND
Text GLabel 800  2650 3    50   Input ~ 0
GND
Text GLabel 800  900  1    50   Input ~ 0
+5V
Wire Wire Line
	1700 2350 800  2350
Connection ~ 800  1200
Text GLabel 1650 1400 0    50   Input ~ 0
Measurement_1
Wire Wire Line
	800  1200 1650 1200
Connection ~ 800  2350
Text GLabel 1700 2550 0    50   Input ~ 0
Measurement_2
$Comp
L 4xxx:4081 U3
U 1 1 5BD90A82
P 3400 5100
F 0 "U3" H 3400 5425 50  0000 C CNN
F 1 "CD4081BM" H 3400 5334 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3400 5100 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 3400 5100 50  0001 C CNN
F 4 "https://www.conrad.de/de/logik-ic-gate-texas-instruments-cd4081bm-and-gate-4000b-soic-14-143502.html#downloadcenter" H 3400 5100 50  0001 C CNN "Bst"
	1    3400 5100
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U3
U 2 1 5BD90B39
P 6100 5350
F 0 "U3" H 6100 5675 50  0000 C CNN
F 1 "CD4081BM" H 6100 5584 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6100 5350 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 6100 5350 50  0001 C CNN
	2    6100 5350
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U3
U 3 1 5BD90BF2
P 12950 950
F 0 "U3" H 12950 1275 50  0000 C CNN
F 1 "CD4081BM" H 12950 1184 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 12950 950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 12950 950 50  0001 C CNN
	3    12950 950 
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U3
U 4 1 5BD90C77
P 13550 1050
F 0 "U3" H 13550 1375 50  0000 C CNN
F 1 "CD4081BM" H 13550 1284 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 13550 1050 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 13550 1050 50  0001 C CNN
	4    13550 1050
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4081 U3
U 5 1 5BD90D05
P 1700 7100
F 0 "U3" V 1950 7050 50  0000 L CNN
F 1 "CD4081BM" V 2050 6900 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 1700 7100 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 1700 7100 50  0001 C CNN
	5    1700 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 4550 2550 4550
Wire Wire Line
	3100 4550 3100 5000
Wire Wire Line
	3100 5200 3100 5600
Wire Wire Line
	3100 5600 2850 5600
$Comp
L 4xxx:4072 U4
U 1 1 5BD88AA9
P 5350 5250
F 0 "U4" H 5350 5625 50  0000 C CNN
F 1 "CD4072BM" H 5350 5534 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5350 5250 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4071bms-72bms-75bms.pdf" H 5350 5250 50  0001 C CNN
F 4 "https://www.conrad.de/de/logik-ic-gate-texas-instruments-cd4072bm-or-gate-4000b-soic-14-1015515.html" H 5350 5250 50  0001 C CNN "Bst"
	1    5350 5250
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4072 U4
U 2 1 5BD88B3C
P 4700 1450
F 0 "U4" H 4700 1825 50  0000 C CNN
F 1 "CD4072BM" H 4700 1734 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4700 1450 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4071bms-72bms-75bms.pdf" H 4700 1450 50  0001 C CNN
	2    4700 1450
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4072 U4
U 3 1 5BD88BEA
P 2300 7100
F 0 "U4" V 2550 7050 50  0000 L CNN
F 1 "CD4072BM" V 2650 6900 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2300 7100 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4071bms-72bms-75bms.pdf" H 2300 7100 50  0001 C CNN
	3    2300 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3700 5100 5050 5100
$Comp
L Device:R R15
U 1 1 5BD8A658
P 6700 4000
F 0 "R15" H 6770 4046 50  0000 L CNN
F 1 "18k" H 6770 3955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6630 4000 50  0001 C CNN
F 3 "~" H 6700 4000 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-18-kohm-100-mw-1-smd-0603-18k-p89445.html?&trstct=pos_0" H 6700 4000 50  0001 C CNN "Bst"
	1    6700 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R16
U 1 1 5BD8A872
P 6700 4300
F 0 "R16" H 6770 4346 50  0000 L CNN
F 1 "4,7k" H 6770 4255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6630 4300 50  0001 C CNN
F 3 "~" H 6700 4300 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-4-7-kohm-100-mw-1-smd-0603-4-7k-p89438.html?&trstct=pos_0" H 6700 4300 50  0001 C CNN "Bst"
	1    6700 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R17
U 1 1 5BD8BBAC
P 7150 4000
F 0 "R17" H 7220 4046 50  0000 L CNN
F 1 "18k" H 7220 3955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7080 4000 50  0001 C CNN
F 3 "~" H 7150 4000 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-18-kohm-100-mw-1-smd-0603-18k-p89445.html?&trstct=pos_0" H 7150 4000 50  0001 C CNN "Bst"
	1    7150 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R18
U 1 1 5BD8BBB3
P 7150 4300
F 0 "R18" H 7220 4346 50  0000 L CNN
F 1 "4,7k" H 7220 4255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7080 4300 50  0001 C CNN
F 3 "~" H 7150 4300 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-4-7-kohm-100-mw-1-smd-0603-4-7k-p89438.html?&trstct=pos_0" H 7150 4300 50  0001 C CNN "Bst"
	1    7150 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R19
U 1 1 5BD8C137
P 7600 4000
F 0 "R19" H 7670 4046 50  0000 L CNN
F 1 "18k" H 7670 3955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7530 4000 50  0001 C CNN
F 3 "~" H 7600 4000 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-18-kohm-100-mw-1-smd-0603-18k-p89445.html?&trstct=pos_0" H 7600 4000 50  0001 C CNN "Bst"
	1    7600 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R20
U 1 1 5BD8C13E
P 7600 4300
F 0 "R20" H 7670 4346 50  0000 L CNN
F 1 "4,7k" H 7670 4255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7530 4300 50  0001 C CNN
F 3 "~" H 7600 4300 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-4-7-kohm-100-mw-1-smd-0603-4-7k-p89438.html?&trstct=pos_0" H 7600 4300 50  0001 C CNN "Bst"
	1    7600 4300
	1    0    0    -1  
$EndComp
Text GLabel 6750 4150 2    50   Input ~ 0
Air_1
Text GLabel 5650 3750 0    50   Input ~ 0
Air_2
Text GLabel 7650 4150 2    50   Input ~ 0
PC
Wire Wire Line
	6700 3000 6700 3850
Wire Wire Line
	6800 3000 6800 3550
Wire Wire Line
	6800 3550 7150 3550
Wire Wire Line
	7150 3550 7150 3850
Wire Wire Line
	7600 3850 7600 3450
Wire Wire Line
	7600 3450 6900 3450
Wire Wire Line
	6900 3450 6900 3000
Wire Wire Line
	6700 4150 6750 4150
Connection ~ 6700 4150
Wire Wire Line
	7150 4150 7200 4150
Connection ~ 7150 4150
Wire Wire Line
	7600 4150 7650 4150
Connection ~ 7600 4150
Wire Wire Line
	7600 4450 7150 4450
Wire Wire Line
	6700 4450 7150 4450
Connection ~ 7150 4450
Text GLabel 7150 4550 3    50   Input ~ 0
GND
Wire Wire Line
	7150 4550 7150 4450
$Comp
L 4xxx:4069 U5
U 1 1 5BD90C55
P 3900 5950
F 0 "U5" H 3900 6267 50  0000 C CNN
F 1 "CD4069UBM" H 3900 6176 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3900 5950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 3900 5950 50  0001 C CNN
F 4 "https://www.conrad.de/de/logik-ic-inverter-texas-instruments-cd4069ubm96-inverter-4000b-soic-14-1073249.html#downloadcenter" H 3900 5950 50  0001 C CNN "Bst"
	1    3900 5950
	0    -1   -1   0   
$EndComp
$Comp
L 4xxx:4069 U5
U 2 1 5BD90EA6
P 4400 5950
F 0 "U5" H 4400 6267 50  0000 C CNN
F 1 "CD4069UBM" H 4400 6176 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4400 5950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 4400 5950 50  0001 C CNN
	2    4400 5950
	0    -1   -1   0   
$EndComp
$Comp
L 4xxx:4069 U5
U 3 1 5BD90FAE
P 4900 5950
F 0 "U5" H 4900 6267 50  0000 C CNN
F 1 "CD4069UBM" H 4900 6176 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4900 5950 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 4900 5950 50  0001 C CNN
	3    4900 5950
	0    -1   -1   0   
$EndComp
$Comp
L 4xxx:4069 U5
U 4 1 5BD9106A
P 3450 3550
F 0 "U5" H 3450 3867 50  0000 C CNN
F 1 "CD4069UBM" H 3450 3776 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3450 3550 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 3450 3550 50  0001 C CNN
	4    3450 3550
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4069 U5
U 5 1 5BD9112C
P 3450 4100
F 0 "U5" H 3450 4417 50  0000 C CNN
F 1 "CD4069UBM" H 3450 4326 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3450 4100 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 3450 4100 50  0001 C CNN
	5    3450 4100
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4069 U5
U 6 1 5BD911DF
P 5850 1450
F 0 "U5" H 5850 1767 50  0000 C CNN
F 1 "CD4069UBM" H 5850 1676 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5850 1450 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 5850 1450 50  0001 C CNN
	6    5850 1450
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4069 U5
U 7 1 5BD9129F
P 2900 7100
F 0 "U5" V 3150 7050 50  0000 L CNN
F 1 "CD4069UBM" V 3250 6900 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2900 7100 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4069ubms.pdf" H 2900 7100 50  0001 C CNN
	7    2900 7100
	1    0    0    -1  
$EndComp
Text GLabel 3900 6250 3    50   Input ~ 0
Air_1
Text GLabel 7200 4150 2    50   Input ~ 0
Air_2
Text GLabel 4900 6250 3    50   Input ~ 0
PC
Wire Wire Line
	5050 5200 3900 5200
Wire Wire Line
	3900 5200 3900 5650
Wire Wire Line
	4400 5650 4400 5300
Wire Wire Line
	4400 5300 5050 5300
Wire Wire Line
	5050 5400 4900 5400
Wire Wire Line
	4900 5400 4900 5650
Text GLabel 7450 6200 3    50   Input ~ 0
GND
Text GLabel 800  5400 1    50   Input ~ 0
+5V
Wire Wire Line
	3150 3550 2550 3550
Wire Wire Line
	2550 3550 2550 4550
Connection ~ 2550 4550
Wire Wire Line
	2550 4550 3100 4550
Wire Wire Line
	3150 4100 2850 4100
Wire Wire Line
	2850 4100 2850 5600
Connection ~ 2850 5600
Wire Wire Line
	2850 5600 2250 5600
$Comp
L 4xxx:4073 U6
U 1 1 5BDA10EC
P 5250 3650
F 0 "U6" H 5250 3975 50  0000 C CNN
F 1 "CD4073BM" H 5250 3884 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5250 3650 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 5250 3650 50  0001 C CNN
F 4 "https://www.conrad.de/de/logik-ic-gate-texas-instruments-cd4073bm-and-gate-4000b-soic-14-1015518.html" H 5250 3650 50  0001 C CNN "Bst"
	1    5250 3650
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4073 U6
U 2 1 5BDA11B0
P 5950 3750
F 0 "U6" H 5950 4075 50  0000 C CNN
F 1 "CD4073BM" H 5950 3984 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5950 3750 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 5950 3750 50  0001 C CNN
	2    5950 3750
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4073 U6
U 3 1 5BDA127C
P 12750 2600
F 0 "U6" H 12750 2925 50  0000 C CNN
F 1 "CD4073BM" H 12750 2834 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 12750 2600 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 12750 2600 50  0001 C CNN
	3    12750 2600
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4073 U6
U 4 1 5BDA134A
P 3500 7100
F 0 "U6" V 3750 7050 50  0000 L CNN
F 1 "CD4073BM" V 3850 6900 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3500 7100 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4073bms-81bms-82bms.pdf" H 3500 7100 50  0001 C CNN
	4    3500 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3550 4950 3550
Wire Wire Line
	3750 4100 4200 4100
Wire Wire Line
	4200 4100 4200 3650
Wire Wire Line
	4200 3650 4950 3650
Text GLabel 4950 3750 0    50   Input ~ 0
Air_1
Text GLabel 4400 6250 3    50   Input ~ 0
Air_2
Wire Wire Line
	5550 3650 5650 3650
Text GLabel 5650 3850 0    50   Input ~ 0
PC
Wire Wire Line
	2250 1300 4400 1300
Wire Wire Line
	2300 2450 2700 2450
Wire Wire Line
	2700 2450 2700 1400
Wire Wire Line
	2700 1400 4400 1400
$Comp
L 4xxx:4070 U7
U 1 1 5BDAF702
P 3450 2900
F 0 "U7" H 3450 3225 50  0000 C CNN
F 1 "CD4070BM" H 3450 3134 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 3450 2900 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4070bms-77bms.pdf" H 3450 2900 50  0001 C CNN
F 4 "https://www.conrad.de/de/logik-ic-gate-und-inverter-texas-instruments-cd4070bm-xor-exclusive-or-4000b-soic-14-1015510.html" H 3450 2900 50  0001 C CNN "Bst"
	1    3450 2900
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4070 U7
U 2 1 5BDAF7CD
P 12000 -650
F 0 "U7" H 12000 -325 50  0000 C CNN
F 1 "CD4070BM" H 12000 -416 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 12000 -650 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4070bms-77bms.pdf" H 12000 -650 50  0001 C CNN
	2    12000 -650
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4070 U7
U 3 1 5BDAF8A3
P 12600 -650
F 0 "U7" H 12600 -325 50  0000 C CNN
F 1 "CD4070BM" H 12600 -416 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 12600 -650 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4070bms-77bms.pdf" H 12600 -650 50  0001 C CNN
	3    12600 -650
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4070 U7
U 4 1 5BDAF981
P 13250 -650
F 0 "U7" H 13250 -325 50  0000 C CNN
F 1 "CD4070BM" H 13250 -416 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 13250 -650 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4070bms-77bms.pdf" H 13250 -650 50  0001 C CNN
	4    13250 -650
	1    0    0    -1  
$EndComp
$Comp
L 4xxx:4070 U7
U 5 1 5BDAFA60
P 4100 7100
F 0 "U7" V 4350 7050 50  0000 L CNN
F 1 "CD4070BM" V 4450 6900 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4100 7100 50  0001 C CNN
F 3 "http://www.intersil.com/content/dam/Intersil/documents/cd40/cd4070bms-77bms.pdf" H 4100 7100 50  0001 C CNN
	5    4100 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2800 2550 2800
Wire Wire Line
	2550 2800 2550 3550
Connection ~ 2550 3550
Wire Wire Line
	2850 4100 2850 3000
Wire Wire Line
	2850 3000 3150 3000
Connection ~ 2850 4100
Wire Wire Line
	3750 2900 4100 2900
Wire Wire Line
	4100 2900 4100 1500
Wire Wire Line
	4100 1500 4400 1500
Text Label 5000 1450 0    50   ~ 0
Safe_State=1
Text GLabel 11000 2200 2    50   Input ~ 0
Red
Text GLabel 6250 3750 2    50   Input ~ 0
Green
Wire Wire Line
	5000 1450 5550 1450
Text GLabel 6150 1450 2    50   Input ~ 0
Safe_State
$Comp
L Transistor_BJT:BC818 Q1
U 1 1 5BDC37A5
P 6950 5850
F 0 "Q1" H 7140 5896 50  0000 L CNN
F 1 "BC818" H 7140 5805 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7150 5775 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/BC/BC818.pdf" H 6950 5850 50  0001 L CNN
F 4 "https://www.reichelt.de/bipolartransistor-npn-25v-1a-0-25w-sot-23-bc-818-16-smd-p18556.html" H 6950 5850 50  0001 C CNN "Bst"
	1    6950 5850
	-1   0    0    -1  
$EndComp
$Comp
L Transistor_BJT:BC818 Q2
U 1 1 5BDC3966
P 7900 5850
F 0 "Q2" H 8091 5896 50  0000 L CNN
F 1 "BC818" H 8091 5805 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8100 5775 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/BC/BC818.pdf" H 7900 5850 50  0001 L CNN
F 4 "https://www.reichelt.de/bipolartransistor-npn-25v-1a-0-25w-sot-23-bc-818-16-smd-p18556.html" H 7900 5850 50  0001 C CNN "Bst"
	1    7900 5850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R12
U 1 1 5BDC3B2D
P 7150 5200
F 0 "R12" H 7220 5246 50  0000 L CNN
F 1 "220k" H 7220 5155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7080 5200 50  0001 C CNN
F 3 "~" H 7150 5200 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-220-kohm-100-mw-1-smd-0603-220k-p89458.html?&trstct=pos_0" H 7150 5200 50  0001 C CNN "Bst"
	1    7150 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5BDC3D59
P 7000 5550
F 0 "C3" V 6748 5550 50  0000 C CNN
F 1 "1uF" V 6839 5550 50  0000 C CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 7038 5400 50  0001 C CNN
F 3 "~" H 7000 5550 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-folienkondensator-1-0-uf-16-v-ecpu-1-0u-16-p200458.html?&trstct=pos_5" V 7000 5550 50  0001 C CNN "Bst"
	1    7000 5550
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 5BDC5805
P 6850 5200
F 0 "R11" H 6920 5246 50  0000 L CNN
F 1 "1k" H 6920 5155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 6780 5200 50  0001 C CNN
F 3 "~" H 6850 5200 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-1-0-kohm-63-mw-0-1-wel-pcf0603r-1k-p238054.html?&trstct=pos_0" H 6850 5200 50  0001 C CNN "Bst"
	1    6850 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5BDCB1FE
P 7850 5550
F 0 "C4" V 7598 5550 50  0000 C CNN
F 1 "1uF" V 7689 5550 50  0000 C CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 7888 5400 50  0001 C CNN
F 3 "~" H 7850 5550 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-folienkondensator-1-0-uf-16-v-ecpu-1-0u-16-p200458.html?&trstct=pos_5" V 7850 5550 50  0001 C CNN "Bst"
	1    7850 5550
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 5BDCB3AC
P 7700 5200
F 0 "R13" H 7770 5246 50  0000 L CNN
F 1 "220k" H 7770 5155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7630 5200 50  0001 C CNN
F 3 "~" H 7700 5200 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-220-kohm-100-mw-1-smd-0603-220k-p89458.html?&trstct=pos_0" H 7700 5200 50  0001 C CNN "Bst"
	1    7700 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5BDCB473
P 8000 5200
F 0 "R14" H 8070 5246 50  0000 L CNN
F 1 "1k" H 8070 5155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7930 5200 50  0001 C CNN
F 3 "~" H 8000 5200 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-1-0-kohm-63-mw-0-1-wel-pcf0603r-1k-p238054.html?&trstct=pos_0" H 8000 5200 50  0001 C CNN "Bst"
	1    8000 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 5050 7150 5050
Connection ~ 7150 5050
Wire Wire Line
	7700 5050 8000 5050
Connection ~ 7700 5050
Wire Wire Line
	8000 5350 8000 5550
Wire Wire Line
	8000 5550 8000 5650
Connection ~ 8000 5550
Wire Wire Line
	7700 5350 7700 5550
Wire Wire Line
	6850 5350 6850 5550
Wire Wire Line
	6850 5550 6850 5650
Connection ~ 6850 5550
Wire Wire Line
	6850 6050 6850 6150
Wire Wire Line
	6850 6150 7450 6150
Wire Wire Line
	8000 6150 8000 6050
Wire Wire Line
	7150 5850 7450 5850
Wire Wire Line
	7450 5850 7450 5550
Wire Wire Line
	7450 5550 7700 5550
Connection ~ 7700 5550
Wire Wire Line
	7700 5850 7600 5850
Wire Wire Line
	7600 5850 7600 5700
Wire Wire Line
	7600 5700 7150 5700
Wire Wire Line
	7150 5700 7150 5550
Wire Wire Line
	7150 5550 7150 5350
Connection ~ 7150 5550
Text GLabel 7450 5000 1    50   Input ~ 0
+5V
Text GLabel 850  7600 0    50   Input ~ 0
GND
Wire Wire Line
	7150 5050 7450 5050
Wire Wire Line
	7450 5000 7450 5050
Connection ~ 7450 5050
Wire Wire Line
	7450 5050 7700 5050
Wire Wire Line
	7450 6200 7450 6150
Connection ~ 7450 6150
Wire Wire Line
	7450 6150 8000 6150
Text GLabel 8150 5550 2    50   Input ~ 0
Clock
Wire Wire Line
	8150 5550 8000 5550
$Comp
L Device:Q_NMOS_GSD Q4
U 1 1 5BDF350D
P 9450 2200
F 0 "Q4" H 9655 2246 50  0000 L CNN
F 1 "IRLML 2060" H 9655 2155 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 9650 2300 50  0001 C CNN
F 3 "~" H 9450 2200 50  0001 C CNN
F 4 "https://www.reichelt.de/mosfet-n-ch-60v-1-2a-1-25w-sot-23-irlml-2060-p132142.html?&trstct=pol_7" H 9450 2200 50  0001 C CNN " Bst"
	1    9450 2200
	1    0    0    -1  
$EndComp
Text GLabel 9250 2200 0    50   Input ~ 0
Green
Wire Wire Line
	5800 5250 5650 5250
Text GLabel 5800 5450 0    50   Input ~ 0
Clock
$Comp
L Device:Q_NMOS_GSD Q5
U 1 1 5BE0281B
P 10800 2200
F 0 "Q5" H 11006 2246 50  0000 L CNN
F 1 "IRLML 2060" H 11006 2155 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 11000 2300 50  0001 C CNN
F 3 "~" H 10800 2200 50  0001 C CNN
F 4 "https://www.reichelt.de/mosfet-n-ch-60v-1-2a-1-25w-sot-23-irlml-2060-p132142.html?&trstct=pol_7" H 10800 2200 50  0001 C CNN " Bst"
	1    10800 2200
	-1   0    0    -1  
$EndComp
Text GLabel 6400 5350 2    50   Input ~ 0
Red
Wire Wire Line
	9550 2400 9550 2550
Wire Wire Line
	9550 2550 10150 2550
Wire Wire Line
	10700 2550 10700 2400
$Comp
L Device:Q_NMOS_GSD Q6
U 1 1 5BE05468
P 10050 3000
F 0 "Q6" H 10255 3046 50  0000 L CNN
F 1 "IRLML 2060" H 10255 2955 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 10250 3100 50  0001 C CNN
F 3 "~" H 10050 3000 50  0001 C CNN
F 4 "https://www.reichelt.de/mosfet-n-ch-60v-1-2a-1-25w-sot-23-irlml-2060-p132142.html?&trstct=pol_7" H 10050 3000 50  0001 C CNN " Bst"
	1    10050 3000
	1    0    0    -1  
$EndComp
Connection ~ 10150 2550
Wire Wire Line
	10150 2550 10700 2550
Wire Wire Line
	10150 2550 10150 2800
$Comp
L Device:C C5
U 1 1 5BD8C7C0
P 9500 3150
F 0 "C5" H 9615 3196 50  0000 L CNN
F 1 "10uF" H 9615 3105 50  0000 L CNN
F 2 "Capacitor_SMD:C_1210_3225Metric" H 9538 3000 50  0001 C CNN
F 3 "~" H 9500 3150 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-kerko-1210-10-f-16v-10-mlcc-x5r-1210-cd-10u-p194469.html?&trstct=pos_6" H 9500 3150 50  0001 C CNN "Bst"
	1    9500 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R21
U 1 1 5BD8C8CB
P 8050 3000
F 0 "R21" V 7843 3000 50  0000 C CNN
F 1 "100k" V 7934 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7980 3000 50  0001 C CNN
F 3 "~" H 8050 3000 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-100-kohm-100-mw-1-smd-0603-100k-p89454.html?&trstct=pos_0" V 8050 3000 50  0001 C CNN "Bst"
	1    8050 3000
	0    1    1    0   
$EndComp
$Comp
L Device:D D1
U 1 1 5BD9783D
P 8050 2700
F 0 "D1" H 8050 2916 50  0000 C CNN
F 1 "1N4001" H 8050 2825 50  0000 C CNN
F 2 "Diode_SMD:D_MELF" H 8050 2700 50  0001 C CNN
F 3 "~" H 8050 2700 50  0001 C CNN
F 4 "https://www.reichelt.de/gleichrichterdiode-50-v-1-a-do-213ab-1n-4001-smd-p18396.html?;ARTICLE=18396&&r=1" H 8050 2700 50  0001 C CNN "Bst"
	1    8050 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7900 2700 7900 3000
Wire Wire Line
	8200 2700 8200 3000
Wire Wire Line
	9500 3000 9850 3000
Connection ~ 9500 3000
Wire Wire Line
	10150 3200 10150 3300
Wire Wire Line
	10150 3300 9850 3300
Text GLabel 10350 6250 0    50   Input ~ 0
GND
Wire Wire Line
	9850 3400 9850 3300
Connection ~ 9850 3300
Wire Wire Line
	9850 3300 9500 3300
Text GLabel 7750 3000 0    50   Input ~ 0
Safe_State
Wire Wire Line
	7750 3000 7900 3000
Connection ~ 7900 3000
Text GLabel 9850 3400 3    50   Input ~ 0
GND
Text GLabel 10350 6150 0    50   Input ~ 0
+24V
Wire Wire Line
	3500 7600 4100 7600
Wire Wire Line
	3500 7600 2900 7600
Connection ~ 3500 7600
Wire Wire Line
	2900 7600 2300 7600
Connection ~ 2900 7600
Wire Wire Line
	2300 7600 1700 7600
Connection ~ 2300 7600
Wire Wire Line
	1700 7600 1050 7600
Connection ~ 1700 7600
Wire Wire Line
	1050 7600 1050 7450
Connection ~ 1050 7600
Wire Wire Line
	1050 7600 850  7600
Wire Wire Line
	4100 6600 3500 6600
Wire Wire Line
	3500 6600 2900 6600
Connection ~ 3500 6600
Wire Wire Line
	2900 6600 2300 6600
Connection ~ 2900 6600
Wire Wire Line
	2300 6600 1700 6600
Connection ~ 2300 6600
Wire Wire Line
	1700 6600 1050 6600
Connection ~ 1700 6600
Wire Wire Line
	1050 6850 1050 6600
Connection ~ 1050 6600
Wire Wire Line
	1050 6600 850  6600
Text Notes 9250 6250 0    50   ~ 0
Spannungsversorgung\n
Text GLabel 10350 5250 0    50   Input ~ 0
A-F
Text Notes 8700 600  0    50   ~ 0
Pinbelegung nach Datenblatt PNP-Leuchte\n
$Comp
L Device:Q_NMOS_GSD Q3
U 1 1 5BE99E44
P 8250 2200
F 0 "Q3" H 8455 2246 50  0000 L CNN
F 1 "IRLML 2060" H 8455 2155 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8450 2300 50  0001 C CNN
F 3 "~" H 8250 2200 50  0001 C CNN
F 4 "https://www.reichelt.de/mosfet-n-ch-60v-1-2a-1-25w-sot-23-irlml-2060-p132142.html?&trstct=pol_7" H 8250 2200 50  0001 C CNN " Bst"
	1    8250 2200
	1    0    0    -1  
$EndComp
Text GLabel 8050 2200 0    50   Input ~ 0
Green
Wire Wire Line
	8350 2400 8350 2550
Wire Wire Line
	8200 3000 9500 3000
Connection ~ 8200 3000
Text Notes 6900 1800 0    50   ~ 0
Cockpit LED TSAL OFF\n
$Comp
L Device:R R22
U 1 1 5BEAE3DA
P 7900 1900
F 0 "R22" V 7693 1900 50  0000 C CNN
F 1 "220" V 7784 1900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7830 1900 50  0001 C CNN
F 3 "~" H 7900 1900 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-220-ohm-125-mw-1-smd-0805-220-p32878.html?&trstct=pos_3" V 7900 1900 50  0001 C CNN "Bst"
	1    7900 1900
	0    1    1    0   
$EndComp
Text GLabel 7750 1900 0    50   Input ~ 0
+5V
Wire Wire Line
	8050 1900 8200 1900
Text Notes 8900 4000 2    50   ~ 0
Messkreis\n
Text Notes 6200 2250 0    50   ~ 0
Pegelwandler 24V=>5V\n
Text Notes 7350 4700 0    50   ~ 0
Astabile Kippstufe 3Hz\n
Text Notes 7900 3200 0    50   ~ 0
Einschaltverzögerung
Text Notes 4450 900  0    50   ~ 0
Safe State Schaltungsteil
Text Notes 5000 3100 0    50   ~ 0
Grünes Licht\nSchaltungsteil
Text Notes 5050 4750 0    50   ~ 0
Rotes Licht \nSchaltungsteil
Text Notes 1900 6450 0    50   ~ 0
Spannungsversorgung CMOS
Text Notes 1000 700  0    50   ~ 0
Komperatoren Messkreis
Text Notes 3750 5600 0    50   ~ 0
Hilfskontakt muss 0V geben für Rot\n
Wire Wire Line
	4400 1600 4400 1500
Connection ~ 4400 1500
Text GLabel 6700 3000 1    50   Input ~ 0
Air_1_Connector
Text GLabel 6800 3000 1    50   Input ~ 0
Air_2_Connector
Text GLabel 6900 3000 1    50   Input ~ 0
PC_Connector
Text GLabel 10350 3850 0    50   Input ~ 0
Air_1_Connector
Text GLabel 10350 3950 0    50   Input ~ 0
Air_2_Connector
Text GLabel 10350 4050 0    50   Input ~ 0
PC_Connector
Connection ~ 9250 4400
Connection ~ 9250 4100
Text GLabel 8750 2000 2    50   Input ~ 0
Green_Connector
Text GLabel 10350 5150 0    50   Input ~ 0
Red_Connector
Text GLabel 10350 4950 0    50   Input ~ 0
GND_Lamp_Connector
Text GLabel 10350 5050 0    50   Input ~ 0
Green_Connector
Text GLabel 9900 2000 2    50   Input ~ 0
Red_Connector
Text GLabel 10350 5950 0    50   Input ~ 0
Cockpit_LED_Connector_GND
Wire Wire Line
	8350 2550 9550 2550
Connection ~ 9550 2550
Text GLabel 10350 6050 0    50   Input ~ 0
Cockpit_LED_Connector_Vcc
Wire Wire Line
	8300 1450 8350 1450
Wire Wire Line
	8350 1450 8350 2000
Wire Wire Line
	8200 1600 8200 1900
Text GLabel 8300 1450 0    50   Input ~ 0
Cockpit_LED_Connector_GND
Text GLabel 8200 1600 0    50   Input ~ 0
Cockpit_LED_Connector_Vcc
$Comp
L Connector:DB25_Female_MountingHoles J1
U 1 1 5C337F06
P 10650 5050
F 0 "J1" H 10830 5053 50  0000 L CNN
F 1 "DB25_Female_MountingHoles" H 9250 3650 50  0000 L CNN
F 2 "Connector:D-Sub25_Cure" H 10650 5050 50  0001 C CNN
F 3 " ~" H 10650 5050 50  0001 C CNN
	1    10650 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 4100 9400 4250
Wire Wire Line
	9400 4250 10350 4250
Wire Wire Line
	10350 4350 9400 4350
Wire Wire Line
	9400 4350 9400 4400
Wire Wire Line
	9400 4650 10350 4650
Wire Wire Line
	9400 4750 10350 4750
$Comp
L Cure_CM19e:G3VM-41DY1 K1
U 1 1 5C294998
P 9550 1700
F 0 "K1" H 9950 1965 50  0000 C CNN
F 1 "G3VM-41DY1" H 9950 1874 50  0000 C CNN
F 2 "SOP254P1000X380-4N" H 10200 1800 50  0001 L CNN
F 3 "https://www.omron.com/ecb/products/pdf/en-g3vm_ay_dy.pdf" H 10200 1700 50  0001 L CNN
F 4 "OMRON ELECTRONIC COMPONENTS - G3VM-41DY1 - MOSFET RELAY, SPST-NO, 40V, 2A" H 10200 1600 50  0001 L CNN "Description"
F 5 "3.8" H 10200 1500 50  0001 L CNN "Height"
F 6 "Omron Electronics" H 10200 1400 50  0001 L CNN "Manufacturer_Name"
F 7 "G3VM-41DY1" H 10200 1300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "G3VM-41DY1" H 10200 1000 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/g3vm-41dy1/omron" H 10200 900 50  0001 L CNN "Arrow Price/Stock"
	1    9550 1700
	-1   0    0    -1  
$EndComp
$Comp
L Cure_CM19e:G3VM-41DY1 K2
U 1 1 5C2E603D
P 10700 1700
F 0 "K2" H 11100 1965 50  0000 C CNN
F 1 "G3VM-41DY1" H 11100 1874 50  0000 C CNN
F 2 "SOP254P1000X380-4N" H 11350 1800 50  0001 L CNN
F 3 "https://www.omron.com/ecb/products/pdf/en-g3vm_ay_dy.pdf" H 11350 1700 50  0001 L CNN
F 4 "OMRON ELECTRONIC COMPONENTS - G3VM-41DY1 - MOSFET RELAY, SPST-NO, 40V, 2A" H 11350 1600 50  0001 L CNN "Description"
F 5 "3.8" H 11350 1500 50  0001 L CNN "Height"
F 6 "Omron Electronics" H 11350 1400 50  0001 L CNN "Manufacturer_Name"
F 7 "G3VM-41DY1" H 11350 1300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "G3VM-41DY1" H 11350 1000 50  0001 L CNN "Arrow Part Number"
F 9 "https://www.arrow.com/en/products/g3vm-41dy1/omron" H 11350 900 50  0001 L CNN "Arrow Price/Stock"
	1    10700 1700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9550 1800 9550 2000
Wire Wire Line
	10700 1800 10700 2000
$Comp
L Device:R R23
U 1 1 5C2F1560
P 9550 1400
F 0 "R23" H 9620 1446 50  0000 L CNN
F 1 "120" H 9620 1355 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 9480 1400 50  0001 C CNN
F 3 "~" H 9550 1400 50  0001 C CNN
	1    9550 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:R R24
U 1 1 5C2F1B81
P 10700 1400
F 0 "R24" H 10770 1446 50  0000 L CNN
F 1 "120" H 10770 1355 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 10630 1400 50  0001 C CNN
F 3 "~" H 10700 1400 50  0001 C CNN
	1    10700 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10700 1550 10700 1700
Wire Wire Line
	9550 1550 9550 1700
Text GLabel 10100 1000 1    50   Input ~ 0
+5V
Wire Wire Line
	9550 1250 9550 1000
Wire Wire Line
	10700 1000 10700 1250
Wire Wire Line
	9550 1000 10700 1000
Connection ~ 5300 6900
Text GLabel 8750 1450 0    50   Input ~ 0
+24V
Text GLabel 9900 1250 2    50   Input ~ 0
+24V
Wire Wire Line
	9900 1250 9900 1700
Wire Wire Line
	9900 1800 9900 2000
Wire Wire Line
	8750 1450 8750 1700
Wire Wire Line
	8750 1800 8750 2000
Text GLabel 8250 900  0    50   Input ~ 0
GND_Lamp_Connector
Text GLabel 8400 900  2    50   Input ~ 0
GND
Wire Wire Line
	8250 900  8400 900 
$EndSCHEMATC
