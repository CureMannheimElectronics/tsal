EESchema Schematic File Version 4
LIBS:TSAL_Messplatine-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Isolator:LTV-354T U4
U 1 1 5BBF90D9
P 5300 4000
F 0 "U4" H 5300 4325 50  0000 C CNN
F 1 "LTV-354T" H 5300 4234 50  0000 C CNN
F 2 "Package_SO:SO-4_4.4x3.6mm_P2.54mm" H 5100 3800 50  0001 L CIN
F 3 "http://optoelectronics.liteon.com/upload/download/DS70-2001-004/S_110_LTV-354T%2020140520.pdf" H 5325 4000 50  0001 L CNN
F 4 "https://www.reichelt.de/optokoppler-ltv-354t-smd-p76167.html" H 5300 4000 50  0001 C CNN "Bst"
	1    5300 4000
	1    0    0    -1  
$EndComp
$Comp
L Diode:1N4001 D3
U 1 1 5BBF94B9
P 4550 3900
F 0 "D3" H 4550 3684 50  0000 C CNN
F 1 "1N4001" H 4550 3775 50  0000 C CNN
F 2 "Diode_SMD:D_MELF" H 4550 3725 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 4550 3900 50  0001 C CNN
F 4 "https://www.reichelt.de/gleichrichterdiode-50-v-1-a-do-213ab-1n-4001-smd-p18396.html?;ARTICLE=18396&&r=1" H 4550 3900 50  0001 C CNN "Bst"
	1    4550 3900
	-1   0    0    1   
$EndComp
$Comp
L Device:R R7
U 1 1 5BBF9610
P 4250 3900
F 0 "R7" V 4043 3900 50  0000 C CNN
F 1 "2,2k" V 4134 3900 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 4180 3900 50  0001 C CNN
F 3 "~" H 4250 3900 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-1206-2-2-kohm-250-mw-1-rnd-1206-1-2-2k-p183384.html?&trstct=pos_1" V 4250 3900 50  0001 C CNN "Bst"
	1    4250 3900
	0    1    1    0   
$EndComp
Text GLabel 2400 6350 2    50   Input ~ 0
HV+
Text GLabel 2400 6750 2    50   Input ~ 0
HV-
Text GLabel 2300 1100 1    50   Input ~ 0
HV+
$Comp
L Device:R R1
U 1 1 5BBFA7C7
P 1050 1050
F 0 "R1" H 1120 1096 50  0000 L CNN
F 1 "330k" H 1120 1005 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 980 1050 50  0001 C CNN
F 3 "" H 1050 1050 50  0001 C CNN
F 4 "https://www.reichelt.de/widerstand-metalloxyd-330-kohm-0207-1-0-w-5-1w-330k-p1815.html?&trstct=pos_0" H 1050 1050 50  0001 C CNN "Bst"
	1    1050 1050
	1    0    0    -1  
$EndComp
$Comp
L Diode:BZX84Cxx D1
U 1 1 5BBFAC3C
P 1050 1500
F 0 "D1" V 1004 1579 50  0000 L CNN
F 1 "MMSZ4706T1G" V 1095 1579 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-123" H 1050 1325 50  0001 C CNN
F 3 "" H 1050 1500 50  0001 C CNN
F 4 "https://de.rs-online.com/web/p/zener-dioden/1453296/" V 1050 1500 50  0001 C CNN "Bst"
	1    1050 1500
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5BBFACC2
P 1600 1500
F 0 "C1" H 1715 1546 50  0000 L CNN
F 1 "10nF" H 1715 1455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1638 1350 50  0001 C CNN
F 3 "~" H 1600 1500 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-kerko-0805-10nf-200v-10-mlcc-x7r-0805-ch-10n-p194262.html?&trstct=pos_6" H 1600 1500 50  0001 C CNN "Bst"
	1    1600 1500
	1    0    0    -1  
$EndComp
Text GLabel 1050 800  1    50   Input ~ 0
HV+
Text GLabel 1050 1800 3    50   Input ~ 0
HV-
Wire Wire Line
	1050 800  1050 900 
Wire Wire Line
	1600 1650 1050 1650
Wire Wire Line
	1050 1650 1050 1800
Connection ~ 1050 1650
Text GLabel 2300 1500 3    50   Input ~ 0
+15V
Text GLabel 3200 3500 1    50   Input ~ 0
+15V
Text GLabel 3200 4300 3    50   Input ~ 0
HV-
Wire Wire Line
	3200 4300 3200 4200
Wire Wire Line
	3200 3500 3200 3600
$Comp
L Device:R R8
U 1 1 5BBFBA84
P 3500 5200
F 0 "R8" H 3570 5246 50  0000 L CNN
F 1 "120k" H 3570 5155 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3430 5200 50  0001 C CNN
F 3 "~" H 3500 5200 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-120-kohm-125-mw-1-smd-0805-120k-p32911.html?&trstct=pos_0" H 3500 5200 50  0001 C CNN "Bst"
	1    3500 5200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5BBFBB87
P 3500 5600
F 0 "R9" H 3570 5646 50  0000 L CNN
F 1 "15k" H 3570 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3430 5600 50  0001 C CNN
F 3 "~" H 3500 5600 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-15-kohm-125-mw-1-rnd-0805-1-15k-p183255.html?&trstct=pos_1" H 3500 5600 50  0001 C CNN "Bst"
	1    3500 5600
	1    0    0    -1  
$EndComp
Text GLabel 3500 4950 1    50   Input ~ 0
+15V
Text GLabel 3500 5850 3    50   Input ~ 0
HV-
Wire Wire Line
	3500 4950 3500 5050
Wire Wire Line
	3500 5350 3500 5400
Wire Wire Line
	3500 5750 3500 5850
Wire Wire Line
	3500 5400 2300 5400
Connection ~ 3500 5400
Wire Wire Line
	3500 5400 3500 5450
$Comp
L Device:R R6
U 1 1 5BBFC3AA
P 3450 3150
F 0 "R6" V 3243 3150 50  0000 C CNN
F 1 "300k" V 3334 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3380 3150 50  0001 C CNN
F 3 "~" H 3450 3150 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-300-kohm-100-mw-1-rnd-0603-1-300k-p183112.html?&trstct=pos_0" V 3450 3150 50  0001 C CNN "Bst"
	1    3450 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5BBFC47D
P 2050 3150
F 0 "R5" V 1843 3150 50  0000 C CNN
F 1 "10k" V 1934 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 1980 3150 50  0001 C CNN
F 3 "~" H 2050 3150 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0603-10-kohm-100-mw-1-rnd-0603-1-10k-p183077.html?&trstct=pos_0" V 2050 3150 50  0001 C CNN "Bst"
	1    2050 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5BBFC4F0
P 1600 3450
F 0 "R4" H 1530 3404 50  0000 R CNN
F 1 "24k" H 1530 3495 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1530 3450 50  0001 C CNN
F 3 "~" H 1600 3450 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-24-kohm-125-mw-1-rnd-0805-1-24k-p183260.html?&trstct=pos_0" H 1600 3450 50  0001 C CNN "Bst"
	1    1600 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	4100 3900 4100 3150
Wire Wire Line
	1900 3150 1600 3150
Text GLabel 2400 6750 2    50   Input ~ 0
HV-
Text GLabel 1600 3700 3    50   Input ~ 0
HV-
Wire Wire Line
	1600 3600 1600 3700
$Comp
L Device:RTRIM R2
U 1 1 5BBFD4D4
P 1600 2450
F 0 "R2" H 1728 2496 50  0000 L CNN
F 1 "100k" H 1728 2405 50  0000 L CNN
F 2 "Potentiometer_THT:Potentiometer_Vishay_T73XW_Horizontal" V 1530 2450 50  0001 C CNN
F 3 "~" H 1600 2450 50  0001 C CNN
F 4 "https://www.reichelt.de/praezisionspotentiometer-25-gaenge-stehend-100-kohm-64w-100k-p2698.html?&trstct=pos_11" H 1600 2450 50  0001 C CNN "Bst"
	1    1600 2450
	1    0    0    -1  
$EndComp
Text GLabel 1050 800  1    50   Input ~ 0
HV+
Text GLabel 1600 2150 1    50   Input ~ 0
HV+
Wire Wire Line
	1600 2150 1600 2300
$Comp
L Diode:BZX84Cxx D2
U 1 1 5BBFDDF0
P 1200 3450
F 0 "D2" V 1154 3529 50  0000 L CNN
F 1 "BZX84C4V7LT1G" V 1245 3529 50  0000 L CNN
F 2 "Diode_SMD:D_SOT-23_ANK" H 1200 3275 50  0001 C CNN
F 3 "https://diotec.com/tl_files/diotec/files/pdf/datasheets/bzx84c2v4.pdf" H 1200 3450 50  0001 C CNN
F 4 "https://de.rs-online.com/web/p/zener-dioden/5450624/" V 1200 3450 50  0001 C CNN "Bst"
	1    1200 3450
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5BBFE026
P 850 3450
F 0 "C2" H 965 3496 50  0000 L CNN
F 1 "150n" H 965 3405 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 888 3300 50  0001 C CNN
F 3 "~" H 850 3450 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-kerko-0805-150nf-50v-20-mlcc-y5v-0805-df-150n-p194311.html?&trstct=pos_2" H 850 3450 50  0001 C CNN "Bst"
	1    850  3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3250 1200 3250
Wire Wire Line
	1200 3250 1200 3300
Wire Wire Line
	850  3300 850  3250
Wire Wire Line
	850  3250 1200 3250
Connection ~ 1200 3250
Wire Wire Line
	850  3600 1200 3600
Wire Wire Line
	1200 3600 1600 3600
Connection ~ 1200 3600
Text GLabel 4900 4100 0    50   Input ~ 0
HV-
Wire Wire Line
	4900 4100 5000 4100
Wire Wire Line
	1550 6750 2400 6750
Wire Wire Line
	1550 6350 2400 6350
$Comp
L Device:R R3
U 1 1 5BC06FD9
P 1600 2950
F 0 "R3" H 1670 2996 50  0000 L CNN
F 1 "750k" H 1670 2905 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 1530 2950 50  0001 C CNN
F 3 "~" H 1600 2950 50  0001 C CNN
F 4 "https://www.reichelt.de/widerstand-metallschicht-750-kohm-0207-0-6-w-1-metall-750k-p11972.html?&trstct=pos_6" H 1600 2950 50  0001 C CNN "Bst"
	1    1600 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 3100 1600 3150
Connection ~ 1600 3150
Wire Wire Line
	1600 2600 1600 2800
Connection ~ 1600 3600
Connection ~ 1600 3250
Wire Wire Line
	1600 3250 1600 3300
Wire Wire Line
	1600 3150 1600 3250
$Comp
L Diode:1N4001 D4
U 1 1 5BC0A623
P 4850 3900
F 0 "D4" H 4850 3684 50  0000 C CNN
F 1 "1N4001" H 4850 3775 50  0000 C CNN
F 2 "Diode_SMD:D_MELF" H 4850 3725 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 4850 3900 50  0001 C CNN
F 4 "https://www.reichelt.de/gleichrichterdiode-50-v-1-a-do-213ab-1n-4001-smd-p18396.html?;ARTICLE=18396&&r=1" H 4850 3900 50  0001 C CNN "Bst"
	1    4850 3900
	-1   0    0    1   
$EndComp
$Comp
L L78S12CV:L78S12CV IC1
U 1 1 5BC0BF4F
P 6350 1250
F 0 "IC1" H 6350 1620 50  0000 C CNN
F 1 "L78S12CV" H 6350 1529 50  0000 C CNN
F 2 "TO254P450X1020X1935-3P" H 6350 1250 50  0001 L BNN
F 3 "Unavailable" H 6350 1250 50  0001 L BNN
F 4 "L78S12 Series 2 A 12 V Fixed Output Positive Voltage Regulator - TO-220" H 6350 1250 50  0001 L BNN "Feld4"
F 5 "L78S12CV" H 6350 1250 50  0001 L BNN "Feld5"
F 6 "None" H 6350 1250 50  0001 L BNN "Feld6"
F 7 "STMicroelectronics" H 6350 1250 50  0001 L BNN "Feld7"
F 8 "TO-220 STMicroelectronics" H 6350 1250 50  0001 L BNN "Feld8"
	1    6350 1250
	1    0    0    -1  
$EndComp
Text GLabel 5550 1850 3    50   Input ~ 0
GND
$Comp
L Device:C C3
U 1 1 5BC0C3A7
P 5250 1400
F 0 "C3" H 5135 1354 50  0000 R CNN
F 1 "330nF" H 5135 1445 50  0000 R CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5288 1250 50  0001 C CNN
F 3 "~" H 5250 1400 50  0001 C CNN
F 4 "https://www.reichelt.de/vielschicht-kerko-330nf-50v-125-c-kem-x7r0805-330n-p207075.html?&trstct=pos_0" H 5250 1400 50  0001 C CNN "Bst"
	1    5250 1400
	-1   0    0    1   
$EndComp
Wire Wire Line
	5250 1250 5650 1250
Wire Wire Line
	5250 1550 5550 1550
Wire Wire Line
	5550 1550 5550 1850
Wire Wire Line
	5650 1450 5550 1450
Wire Wire Line
	5550 1450 5550 1550
Connection ~ 5550 1550
Wire Wire Line
	6300 1950 5700 1950
Wire Wire Line
	5700 1950 5700 1550
Wire Wire Line
	5700 1550 5550 1550
Wire Wire Line
	7050 1250 7050 1950
Wire Wire Line
	7050 1950 6600 1950
$Comp
L Device:C C4
U 1 1 5BC10A67
P 6450 1950
F 0 "C4" V 6702 1950 50  0000 C CNN
F 1 "100nF" V 6611 1950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6488 1800 50  0001 C CNN
F 3 "~" H 6450 1950 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-vielschicht-keramikkondensator-100n-10-x7r-g0805-100n-p31879.html?&trstct=pos_1" H 6450 1950 50  0001 C CNN "Bst"
	1    6450 1950
	0    -1   -1   0   
$EndComp
Text GLabel 5000 1250 0    50   Input ~ 0
24V
Text GLabel 7350 1250 2    50   Input ~ 0
12V
Wire Wire Line
	7050 1250 7350 1250
Connection ~ 7050 1250
Wire Wire Line
	5000 1250 5250 1250
Connection ~ 5250 1250
$Comp
L Device:R R10
U 1 1 5BC12229
P 6300 4000
F 0 "R10" H 6370 4046 50  0000 L CNN
F 1 "51k" H 6370 3955 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6230 4000 50  0001 C CNN
F 3 "~" H 6300 4000 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-51-kohm-125-mw-1-rnd-0805-1-51k-p183268.html?&trstct=pos_0" H 6300 4000 50  0001 C CNN "Bst"
	1    6300 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R11
U 1 1 5BC12342
P 6300 4300
F 0 "R11" H 6370 4346 50  0000 L CNN
F 1 "51k" H 6370 4255 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6230 4300 50  0001 C CNN
F 3 "~" H 6300 4300 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-51-kohm-125-mw-1-rnd-0805-1-51k-p183268.html?&trstct=pos_0" H 6300 4300 50  0001 C CNN "Bst"
	1    6300 4300
	1    0    0    -1  
$EndComp
Text GLabel 6300 3700 1    50   Input ~ 0
12V
Wire Wire Line
	6300 3700 6300 3800
Wire Wire Line
	5600 3900 6000 3900
Wire Wire Line
	6000 3900 6000 3800
Wire Wire Line
	6000 3800 6300 3800
Connection ~ 6300 3800
Wire Wire Line
	6300 3800 6300 3850
Wire Wire Line
	5600 4100 6000 4100
Wire Wire Line
	6000 4100 6000 4150
Wire Wire Line
	6000 4150 6300 4150
Connection ~ 6300 4150
Text GLabel 6300 4500 3    50   Input ~ 0
GND
Wire Wire Line
	6300 4450 6300 4500
Text GLabel 7650 3650 1    50   Input ~ 0
12V
Text GLabel 6300 4500 3    50   Input ~ 0
GND
Text GLabel 7650 4250 3    50   Input ~ 0
GND
Wire Wire Line
	6300 4150 7050 4150
Wire Wire Line
	6750 4250 6750 4800
Wire Wire Line
	6750 4800 8450 4800
Wire Wire Line
	8450 4800 8450 3950
Text GLabel 9800 3450 1    50   Input ~ 0
12V
Text GLabel 9800 4050 3    50   Input ~ 0
GND
$Comp
L Device:R R15
U 1 1 5BC1B2FC
P 9250 5000
F 0 "R15" V 9043 5000 50  0000 C CNN
F 1 "1k" V 9134 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9180 5000 50  0001 C CNN
F 3 "~" H 9250 5000 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-1-0-kohm-125-mw-1-smd-0805-1-00k-p32886.html?&trstct=pos_0" V 9250 5000 50  0001 C CNN "Bst"
	1    9250 5000
	0    1    1    0   
$EndComp
Text GLabel 8550 5000 0    50   Input ~ 0
GND
Wire Wire Line
	10600 5000 9400 5000
Wire Wire Line
	9100 5000 8850 5000
$Comp
L Device:R R16
U 1 1 5BC20BE2
P 8700 5000
F 0 "R16" V 8493 5000 50  0000 C CNN
F 1 "1k" V 8584 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8630 5000 50  0001 C CNN
F 3 "~" H 8700 5000 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-1-0-kohm-125-mw-1-smd-0805-1-00k-p32886.html?&trstct=pos_0" V 8700 5000 50  0001 C CNN "Bst"
	1    8700 5000
	0    1    1    0   
$EndComp
Connection ~ 8850 5000
$Comp
L Device:R R14
U 1 1 5BC22310
P 10750 3750
F 0 "R14" V 10543 3750 50  0000 C CNN
F 1 "1,2k" V 10634 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10680 3750 50  0001 C CNN
F 3 "~" H 10750 3750 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-1-2-kohm-125-mw-1-smd-0805-1-20k-p32887.html?&trstct=pos_1" V 10750 3750 50  0001 C CNN "Bst"
	1    10750 3750
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5BC2267F
P 8600 3950
F 0 "R12" V 8393 3950 50  0000 C CNN
F 1 "2,2k" V 8484 3950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8530 3950 50  0001 C CNN
F 3 "~" H 8600 3950 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-2-2-kohm-125-mw-1-rnd-0805-1-2-2k-p183236.html?&trstct=pos_1" V 8600 3950 50  0001 C CNN "Bst"
	1    8600 3950
	0    1    1    0   
$EndComp
Wire Wire Line
	10600 3750 10600 5000
Wire Wire Line
	8850 4050 8850 5000
Wire Wire Line
	8750 3950 8900 3950
$Comp
L Device:R R13
U 1 1 5BC2C754
P 9900 3200
F 0 "R13" V 9693 3200 50  0000 C CNN
F 1 "1k" V 9784 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9830 3200 50  0001 C CNN
F 3 "~" H 9900 3200 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-1-0-kohm-125-mw-1-smd-0805-1-00k-p32886.html?&trstct=pos_0" V 9900 3200 50  0001 C CNN "Bst"
	1    9900 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	10900 3750 10900 3200
Wire Wire Line
	10900 3200 10050 3200
Wire Wire Line
	9750 3200 8900 3200
Wire Wire Line
	8900 3200 8900 3650
Text GLabel 10900 3750 2    50   Input ~ 0
5-7mA
Text GLabel 9200 1200 0    50   Input ~ 0
5-7mA
Text GLabel 9200 1300 0    50   Input ~ 0
GND
Text GLabel 9200 1100 0    50   Input ~ 0
GND
Text GLabel 9200 1400 0    50   Input ~ 0
24V
Wire Wire Line
	9350 1400 9200 1400
Wire Wire Line
	9200 1300 9350 1300
Wire Wire Line
	9350 1200 9200 1200
Wire Wire Line
	9200 1100 9350 1100
$Comp
L Device:Q_NMOS_GDS Q1
U 1 1 5BC7B0A2
P 2200 1300
F 0 "Q1" H 2406 1346 50  0000 L CNN
F 1 "IXTY2N65X2" H 2406 1255 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-3_TabPin2_Cure_Mosfet_Leistung_3W" H 2400 1400 50  0001 C CNN
F 3 "~" H 2200 1300 50  0001 C CNN
F 4 "https://de.rs-online.com/web/p/transistoren-mosfet/9171476/" H 2200 1300 50  0001 C CNN "Bst"
	1    2200 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 1200 1050 1300
Wire Wire Line
	2000 1300 1600 1300
Connection ~ 1050 1300
Wire Wire Line
	1050 1300 1050 1350
Wire Wire Line
	1600 1350 1600 1300
Connection ~ 1600 1300
Wire Wire Line
	1600 1300 1050 1300
$Comp
L Device:C C5
U 1 1 5BCB7CA0
P 3450 2750
F 0 "C5" H 3565 2796 50  0000 L CNN
F 1 "150n" H 3565 2705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 3488 2600 50  0001 C CNN
F 3 "~" H 3450 2750 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-kerko-0805-150nf-50v-20-mlcc-y5v-0805-df-150n-p194311.html?&trstct=pos_2" H 3450 2750 50  0001 C CNN "Bst"
	1    3450 2750
	0    -1   -1   0   
$EndComp
$Comp
L pspice:OPAMP U1
U 1 1 5BCB980A
P 3300 3900
F 0 "U1" H 3641 3946 50  0000 L CNN
F 1 "OPA170AIDBVT" H 3641 3855 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 3300 3900 50  0001 C CNN
F 3 "" H 3300 3900 50  0001 C CNN
F 4 "https://www.mouser.de/ProductDetail/Texas-Instruments/OPA170AIDBVT/?qs=tEz3BkPb1rzzAmoD96Gp1g%3d%3d&gclid=Cj0KCQjw6fvdBRCbARIsABGZ-vRounuqYWJafqKLB8gtRiK80Pta8f94aWLNJnMzjN8R1XTv5T9unl4aAn5WEALw_wcB" H 3300 3900 50  0001 C CNN "Bst"
	1    3300 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3150 2300 3150
Wire Wire Line
	2300 3800 2300 3150
Wire Wire Line
	2300 4000 3000 4000
Wire Wire Line
	2300 4000 2300 5400
Connection ~ 2300 3150
Wire Wire Line
	2300 3150 3050 3150
Wire Wire Line
	2300 3800 3000 3800
Wire Wire Line
	3600 3900 4100 3900
Connection ~ 4100 3900
$Comp
L pspice:OPAMP U2
U 1 1 5BCD81E6
P 7750 3950
F 0 "U2" H 8091 3996 50  0000 L CNN
F 1 "OPA170AIDBVT" H 8091 3905 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 7750 3950 50  0001 C CNN
F 3 "" H 7750 3950 50  0001 C CNN
F 4 "https://www.mouser.de/ProductDetail/Texas-Instruments/OPA170AIDBVT/?qs=tEz3BkPb1rzzAmoD96Gp1g%3d%3d&gclid=Cj0KCQjw6fvdBRCbARIsABGZ-vRounuqYWJafqKLB8gtRiK80Pta8f94aWLNJnMzjN8R1XTv5T9unl4aAn5WEALw_wcB" H 7750 3950 50  0001 C CNN "Bst"
	1    7750 3950
	1    0    0    -1  
$EndComp
$Comp
L pspice:OPAMP U3
U 1 1 5BCD826B
P 9900 3750
F 0 "U3" H 10241 3796 50  0000 L CNN
F 1 "OPA170AIDBVT" H 10241 3705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 9900 3750 50  0001 C CNN
F 3 "" H 9900 3750 50  0001 C CNN
F 4 "https://www.mouser.de/ProductDetail/Texas-Instruments/OPA170AIDBVT/?qs=tEz3BkPb1rzzAmoD96Gp1g%3d%3d&gclid=Cj0KCQjw6fvdBRCbARIsABGZ-vRounuqYWJafqKLB8gtRiK80Pta8f94aWLNJnMzjN8R1XTv5T9unl4aAn5WEALw_wcB" H 9900 3750 50  0001 C CNN "Bst"
	1    9900 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3850 7050 3850
Wire Wire Line
	7050 3850 7050 4150
Wire Wire Line
	7450 4250 7450 4050
Wire Wire Line
	6750 4250 7450 4250
Wire Wire Line
	8050 3950 8450 3950
Connection ~ 8450 3950
Wire Wire Line
	9600 3650 8900 3650
Connection ~ 8900 3650
Wire Wire Line
	8900 3650 8900 3950
Wire Wire Line
	9600 4050 9600 3850
Wire Wire Line
	8850 4050 9600 4050
Wire Wire Line
	10200 3750 10600 3750
Connection ~ 10600 3750
$Comp
L Device:R R17
U 1 1 5BCA4833
P 1250 2950
F 0 "R17" H 1320 2996 50  0000 L CNN
F 1 "160k" H 1320 2905 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1180 2950 50  0001 C CNN
F 3 "~" H 1250 2950 50  0001 C CNN
F 4 "https://www.reichelt.de/smd-widerstand-0805-160-kohm-125-mw-1-rnd-0805-1-160k-p212715.html?&trstct=pos_0" H 1250 2950 50  0001 C CNN "Bst"
	1    1250 2950
	1    0    0    -1  
$EndComp
Text GLabel 1250 2700 1    50   Input ~ 0
Test1
Wire Wire Line
	1250 2700 1250 2800
Wire Wire Line
	1250 3100 1600 3100
Connection ~ 1600 3100
Text GLabel 2600 7200 2    50   Input ~ 0
Test1
Text GLabel 2600 7300 2    50   Input ~ 0
HV-
$Comp
L Connector:Conn_01x02_Female J4
U 1 1 5BCAAD2E
P 1700 7200
F 0 "J4" H 1594 7385 50  0000 C CNN
F 1 "Conn_01x02_Female" H 1594 7294 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1700 7200 50  0001 C CNN
F 3 "~" H 1700 7200 50  0001 C CNN
F 4 "https://www.reichelt.de/rnd-buchsenleiste-2-pol-rm-2-54-mm-rnd-205-00642-p208868.html?&trstct=pol_13" H 1700 7200 50  0001 C CNN "Bst"
	1    1700 7200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1900 7200 2600 7200
Wire Wire Line
	2600 7300 1900 7300
Wire Wire Line
	3600 3150 3800 3150
Wire Wire Line
	3600 2750 3800 2750
Wire Wire Line
	3800 2750 3800 3150
Connection ~ 3800 3150
Wire Wire Line
	3800 3150 4100 3150
Wire Wire Line
	3300 2750 3050 2750
Wire Wire Line
	3050 2750 3050 3150
Connection ~ 3050 3150
Wire Wire Line
	3050 3150 3300 3150
$Comp
L Cure:1-770967-0 J1
U 1 1 5C0B6634
P 1150 6550
F 0 "J1" H 995 6083 50  0000 C CNN
F 1 "1-770967-0" H 995 6174 50  0000 C CNN
F 2 "Connector:TE_1-770967-0" H 1150 6550 50  0001 L BNN
F 3 "600V" H 1150 6550 50  0001 L BNN
F 4 "Male" H 1150 6550 50  0001 L BNN "Feld4"
F 5 "Header" H 1150 6550 50  0001 L BNN "Feld6"
F 6 "23july2015" H 1150 6550 50  0001 L BNN "Feld8"
F 7 "TE Connectivity" H 1150 6550 50  0001 L BNN "Feld9"
F 8 "yes" H 1150 6550 50  0001 L BNN "Feld10"
F 9 "http://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=770967&DocType=Customer+Drawing&DocLang=English" H 1150 6550 50  0001 L BNN "Feld13"
F 10 "3" H 1150 6550 50  0001 L BNN "Feld14"
F 11 "Revised per ECR-15-010337" H 1150 6550 50  0001 L BNN "Feld16"
F 12 "Mini Universal MATE-N-LOK" H 1150 6550 50  0001 L BNN "Feld18"
F 13 "Tin" H 1150 6550 50  0001 L BNN "Feld19"
F 14 "4.14mm" H 1150 6550 50  0001 L BNN "Feld20"
F 15 "General Purpose" H 1150 6550 50  0001 L BNN "Feld23"
F 16 "Right Angle" H 1150 6550 50  0001 L BNN "Feld24"
F 17 "MANUFACTURER RECOMMENDATIONS" H 1150 6550 50  0001 L BNN "Feld25"
F 18 "CONN HEADER 3POS RTANG .163 TIN" H 1150 6550 50  0001 L BNN "Feld26"
F 19 "G" H 1150 6550 50  0001 L BNN "Feld27"
F 20 "Through Hole" H 1150 6550 50  0001 L BNN "Feld36"
F 21 "1-770967-0" H 1150 6550 50  0001 L BNN "Feld37"
	1    1150 6550
	-1   0    0    1   
$EndComp
$Comp
L Connector:DB9_Female_MountingHoles J3
U 1 1 5C0C2C84
P 9650 1500
F 0 "J3" H 9830 1503 50  0000 L CNN
F 1 "DB9_Female_MountingHoles" H 9830 1412 50  0000 L CNN
F 2 "Connector:D-Sub9_Cure" H 9650 1500 50  0001 C CNN
F 3 " ~" H 9650 1500 50  0001 C CNN
	1    9650 1500
	1    0    0    -1  
$EndComp
Text GLabel 9650 2100 3    50   Input ~ 0
GND
$EndSCHEMATC
